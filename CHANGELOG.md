# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.1.0](https://gitlab.com/banzeh/automatic-changelog/compare/v1.0.0...v1.1.0) (2020-04-26)


### Features

* adicionado arquivo de ci do gitlab ([b36a273](https://gitlab.com/banzeh/automatic-changelog/commit/b36a273269fbdae84c855f30cbc0dc53948e0fcd))

## 1.0.0 (2020-04-26)


### Features

* automatizado o CHANGELOG.md ([e40f13c](https://gitlab.com/banzeh/automatic-changelog/commit/e40f13c4bb9214d3eb7bfe5d981829f6d45ffa6b))
